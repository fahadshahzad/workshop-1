# README #

Introductory Workshop to Enterprise Application Development. 

### What is this repository for? ###
Setup of Accounts on followings:
* Bitbucket(Version Control Tool)
* Postman(Software APIs Testing tool)
* Jira(Software Management Tool.
* Jetbrains(For Ultimate educational Softwares)

### How do I get set up? ###

Setup for Accounts is documented with EAD-1 Story on Jira

### Contribution guidelines ###

* You can't push code direct to admin's Repositories
* You need to fork the repository with your own accounts.
* Make your own branch and push that code in that and then Raise PR(Pull Request) to Master

### Who do I talk to? ###

* You can email: mfahad.bses19@iba-suk.edu.pk
* You can discuss problems by Friday
* You can also discuss problems in Telegram Application Group link is below 
* Telegram Link https://t.me/joinchat/unNXPflzmPE2MGI0
